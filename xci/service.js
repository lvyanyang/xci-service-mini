const helper = require('./helper')
const request = require('./request')

/** 故障报修服务 */
const repairApplyService = {
  selectDicListByCode(dicCode) {
    return request.post('/api/wx/mini/selectDicListByCode', {dicCode})
  },
  selectMemberByOpenid(openid) {
    return request.post('/api/om/wxMiniMember/selectByOpenid', {openid})
  },
  insertRepairApply(data) {
    return request.post('/api/om/wxMiniRepairApply/insert', data)
  },
  replyRepairApply(data) {
    return request.post('/api/om/wxMiniRepairApply/reply', data)
  },
  selectRepairApplyById(id) {
    return request.post('/api/om/wxMiniRepairApply/selectById', {id})
  },
  selectRepairApplyMemberPageList(filter) {
    return request.post('/api/om/wxMiniRepairApply/selectMemberPageList', filter, {pageSize: true})
  }
}

const omService = {
  getCarLastGps(id) {
    return request.post('/api/om/wxBasicVehicle/getCarLastGps', {id},{loading:'加载定位信息...'})
  },
  selectVehicleById(id) {
    return request.post('/api/om/wxBasicVehicle/selectById', {id})
  },
  selectVehiclePageList(filter) {
    return request.post('/api/om/wxBasicVehicle/selectPageList', filter, {pageSize: true})
  },
  processVehicleModel(model) {
    helper.setObjectDefaultValue(model)
    if (model.plateNumber && model.plateNumber.length > 2) {
      model.plateNumberDisplay = `${model.plateNumber.substr(0, 2)} • ${model.plateNumber.substr(2)}`
    }
    let clsName = 'plate-other'
    if (model.plateNumber.length === 8) clsName = 'plate-electric'
    else if (model.plateColor === '蓝色') clsName = 'plate-blue'
    else if (model.plateColor === '黄色') clsName = 'plate-yellow'
    else if (model.plateColor === '黄绿色') clsName = 'plate-yellow-green'
    model.plateColorClsName = clsName
    model.statusName = model.status?'有效':'无效'
    model.customerDisplay = `${model.customerName}(${model.customerShortName})`

    if (model.serviceEndDate) {
      const serviceEndDate = helper.parseDate(model.serviceEndDate)
      model.serviceEndDate = helper.formatDate(serviceEndDate, 'yyyy年MM月dd日')
      model.serviceExpired = helper.isExpired(serviceEndDate, helper.nowDate())
      model.serviceExpiredName = model.serviceExpired ? '服务过期' : ''
    }
    if (model.rentEndDate) model.rentEndDate = helper.formatDate(helper.parseDate(model.rentEndDate), 'yyyy年MM月dd日')
    if (model.installDate) model.installDate = helper.formatDate(helper.parseDate(model.installDate), 'yyyy年MM月dd日')
    return model
  }
}

module.exports = {
  repairApply: repairApplyService,
  om: omService
}