Component({
  properties: {
    show: {
      type: Boolean,
      value: false
    },
  },
  data: {
    // 省份简写
    provinces: [
      ['陕', '京', '沪', '浙', '苏', '粤', '鲁', '晋', '冀', '豫'],
      ['川', '渝', '辽', '吉', '黑', '皖', '鄂', '湘', '赣', '闽'],
      ['甘', '宁', '蒙', '津', '贵', '云', '桂', '琼', '青', '新'],
      ['藏', '使', '领', '警', '港', '澳'],
    ],
    // 车牌输入
    numbers: [
      ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
      ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', '学', '挂', 'P'],
      ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z'],
      ['X', 'C', 'V', 'B', 'N', 'M']
    ],
    carnum: [],
    keyboardState: false
  },
  methods: {
    // 选中点击设置
    bindChoose(e) {
      if (this.data.carnum.length >= 8) return
      const arr = [];
      arr[0] = e.target.dataset.val;
      this.data.carnum = this.data.carnum.concat(arr)
      this.setData({
        carnum: this.data.carnum
      })
      this.triggerEvent('change', {
        value: this.data.carnum,
      })
    },
    bindDelChoose() {
      if (this.data.carnum.length != 0) {
        this.data.carnum.splice(this.data.carnum.length - 1, 1);
        this.setData({
          carnum: this.data.carnum
        })
        this.triggerEvent('change', {
          value: this.data.carnum,
        })
      }
    },
    onCancel(){
      this.setData({
        carnum: [],
        keyboardState: false
      })
      this.triggerEvent('change', {
        value: this.data.carnum,
      })
    },
    onConfirm(){
      this.setData({
        keyboardState: false
      })
    },
  },
  observers: {
    show: function (v) {
      this.setData({
        keyboardState: v
      })
    },
  },
});
