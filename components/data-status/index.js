Component({
  properties: {
    status: {
      type: Boolean,
      value: true
    },
    msg: {
      type: String,
      value: null
    },
  }
})
