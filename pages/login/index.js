const helper = require('../../xci/helper')
const request = require('../../xci/request')
const auth = require('../../xci/auth')

Page({
  data: {
    openid: '',
    inputCaptcha: '',
    captchaImage: '',
    captchaVisable: false,
    remind: '加载中',
    help_status: false,
    userid_focus: false,
    passwd_focus: false,
    userid: '',
    passwd: '',
    angle: 0
  },
  async onLoad() {
    this.data.openid = await auth.getOpenid()
  },
  onReady() {
    const _this = this;
    setTimeout(function () {
      _this.setData({
        remind: ''
      });
    }, 1000);
    wx.onAccelerometerChange(function (res) {
      let angle = -(res.x * 30).toFixed(1);
      if (angle > 14) {
        angle = 14;
      } else if (angle < -14) {
        angle = -14;
      }
      if (_this.data.angle !== angle) {
        _this.setData({
          angle: angle
        });
      }
    });
  },
  async onLogin() {
    if (!this.data.userid) {
      await helper.showToast('账号不能为空');
      return
    }
    if (!this.data.passwd) {
      await helper.showToast('密码不能为空');
      return
    }
    if (!this.data.openid) {
      await helper.showToast('无法获取身份信息');
      return
    }

    const account = this.data.userid
    const requireCaptchaResult = await request.post('/api/wx/mini/requireCaptcha', {account})
    if (requireCaptchaResult.data === true) {
      this.setData({inputCaptcha: ''})
      await this.buildCaptchaCore(this.data.userid)
    } else {
      await this.loginCore()
    }
  },
  async onSendCaptcha() {
    if (helper.isBlank(this.data.inputCaptcha)) {
      await helper.showToast('请输入验证码')
      return
    }
    await this.loginCore()
  },
  async onRebuildCaptcha() {
    return this.buildCaptchaCore(this.data.userid)
  },
  async buildCaptchaCore(account) {
    const base64 = await request.post('/api/wx/mini/captcha', {account, type: 'math'}, {responseType: 'arraybuffer'})
    this.setData({
      captchaImage: `data:image/png;base64,${wx.arrayBufferToBase64(base64)}`,
      captchaVisable: true
    })
  },
  async loginCore() {
    const data = {
      account: this.data.userid,
      password: this.data.passwd,
      openid: this.data.openid,
      captcha: this.data.inputCaptcha,
    }
    const userInfo = await auth.login(data)
    if (userInfo) {
      wx.redirectTo({
        url: auth.getLastLoginTargetUrl() || helper.getHomePagePath()
      })
    }
  },
  useridInput: function (e) {
    this.setData({
      userid: e.detail.value
    });
    if (e.detail.value.length >= 7) {
      wx.hideKeyboard();
    }
  },
  passwdInput: function (e) {
    this.setData({
      passwd: e.detail.value
    });
  },
  inputFocus: function (e) {
    if (e.target.id === 'userid') {
      this.setData({
        'userid_focus': true
      });
    } else if (e.target.id === 'passwd') {
      this.setData({
        'passwd_focus': true
      });
    }
  },
  inputBlur: function (e) {
    if (e.target.id === 'userid') {
      this.setData({
        'userid_focus': false
      });
    } else if (e.target.id === 'passwd') {
      this.setData({
        'passwd_focus': false
      });
    }
  },
  tapHelp: function (e) {
    if (e.target.id === 'help') {
      this.hideHelp();
    }
  },
  showHelp: function () {
    this.setData({
      'help_status': true
    });
  },
  hideHelp: function () {
    this.setData({
      'help_status': false
    });
  }
});