const auth = require('../../xci/auth')

Page({
  data: {
    userInfo: null,
    items: [
      {
        title: '故障申报',
        icon: 'https://www.xadjtfb.com/Assets/images/czwytgIco.png',
        url: '../repair-apply/index'
      },
      {
        title: '申报查询',
        icon: 'https://www.xadjtfb.com/Assets/images/czfwpjhdIco.png',
        url: '../repair-apply-search/index'
      },
      {
        title: '车辆查询',
        icon: 'https://www.xadjtfb.com/Assets/images/czfwpjIco1.png',
        url: '../vehicle-search/index'
      }
    ]
  },
  onLoad() {
    const userInfo = auth.getTokenInfo()
    this.setData({userInfo})
  },
  onShow() {

  },
  onClick(e) {
    const url = e.currentTarget.dataset.url
    if (!url) {
      return
    }

    wx.navigateTo({url})
  },
  onUserDetiail() {
    const url = this.data.userInfo ? '../logout/index' : '../login/index'
    wx.navigateTo({url})
  }
})
