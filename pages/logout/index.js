const helper = require('../../xci/helper')
const auth = require('../../xci/auth')

Page({
  data: {
    userInfo: {}
  },
  onLoad() {
    const userInfo = auth.getTokenInfo()
    if (userInfo) userInfo.categoryName = userInfo.category === 0 ? '运营商' : '企业身份'
    this.setData({userInfo})
  },
  async onLogout() {
    const r = await wx.showModal({
      title: '系统提示',
      content: '您确定要注销当前账号吗?'
    })
    if (r.cancel === true) return
    helper.showToast('正在注销...')
    auth.logout()
    setTimeout(() => {
      wx.reLaunch({
        url: helper.getHomePagePath(),
        success() {
          helper.hideToast()
        }
      })
    }, 1000)
  }
})