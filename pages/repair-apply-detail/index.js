const helper = require('../../xci/helper')
const service = require('../../xci/service')

Page({
  data: {
    model: null
  },
  async onLoad(options) {
    const id = options.id
    if (!id) return
    const result = await service.repairApply.selectRepairApplyById(id)
    const model = result.data
    if (model.files) {
      model.files = model.files.split(',').map(p => helper.buildUploadFileUrl(p))
    }
    this.setData({model})
    wx.setNavigationBarTitle({
      title: `${model.plateNumber} - ${model.companyName}`
    })
  },
  onOpenLocation() {
    const model = this.data.model
    if (model && model.reserveLongitude && model.reserveLatitude) {
      wx.openLocation({
        latitude: Number(model.reserveLatitude),
        longitude: Number(model.reserveLongitude)
      })
    }
  }
});