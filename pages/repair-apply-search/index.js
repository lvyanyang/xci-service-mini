const pagingBehavior = require('../../xci/pagingBehavior')
const auth = require('../../xci/auth')
const service = require('../../xci/service')

Page({
  behaviors: [pagingBehavior],
  data: {
    filter: {
      openid: '',
      plateNumber: ''
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    this.data.filter.openid = await auth.getOpenid()
    await this.loadData()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false)
  },

  /**
   * 搜索事件
   * @param e
   */
  async onSearch(e) {
    console.log(e)
    this.data.filter.plateNumber = e.detail.value
    await this.loadData()
  },

  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    const data = {...this.data.filter}
    await this.loadPageData(service.repairApply.selectRepairApplyMemberPageList, data, firstPage)
  },

  onDetail(e) {
    const id = e.currentTarget.dataset.id
    if (!id) return
    wx.navigateTo({
      url: `../repair-apply-detail/index?id=${id}`
    })
  }
})