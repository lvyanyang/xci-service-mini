const helper = require('../../xci/helper')
const app = getApp()

Page({
  data: {
    latitude: null,
    longitude: null,
    markers: [],
  },
  onLoad() {
    const model = app.globalData.vehicle
    const addr = helper.fixLenInsertChar(model.MapAddress,15,'\n')
    this.setData({
      latitude: model.LatitudeValue,
      longitude: model.LongitudeValue,
      markers: [{
        id: model.CarID,
        latitude: model.LatitudeValue,
        longitude: model.LongitudeValue,
        iconPath: '../../static/gps_marker@3x.png',
        width: '34px',
        height: '34px',
        rotate: 0,
        callout: {
          content: `
${model.PlateNo}\n
GPS时间：${model.GpsTime}\n
上报时间：${model.CreatedTime}\n
当前车速：${model.Speed}公里/小时 ${model.DirectionText}\n
经纬度：${model.Longitude},${model.Latitude}\n
车辆状态：${model.IsOnline ? '在线' : '离线'}\n
当前位置：\n${addr}
`,
          // content:`${model.PlateNo}`,
          textAlign: 'left',
          padding: 10,
          borderRadius: 4,
          color: '#333333',
          display: 'BYCLICK',
          fontSize:'14',
          anchorX:0,
          anchorY:0
        }
      }]
    })

  }
})