const pagingBehavior = require('../../xci/pagingBehavior')
const helper = require('../../xci/helper')
const auth = require('../../xci/auth')
const service = require('../../xci/service')

Page({
  behaviors: [pagingBehavior],
  data: {
    filter: {
      openid: '',
      key: ''
    },
    filterParams:{},
    filterItems: [
      {
        type: 'sort',
        label: '服务截止日期',
        value: 'service_end_date',
        groups: ['001']
      }, {
        type: 'sort',
        label: '设备安装日期',
        value: 'install_date',
        groups: ['002']
      },
      {
        type: 'filter',
        label: '筛选条件',
        value: 'filter',
        children: [
          {
            type: 'radio',
            label: '业态类型',
            value: 'category',
            children: [{
              label: '班线',
              value: '班线',
            }, {
              label: '旅游',
              value: '旅游',
            }]
          },
          {
            type: 'radio',
            label: '启用状态',
            value: 'status',
            children: [{
              label: '启用',
              value: 'true',
            }, {
              label: '禁用',
              value: 'false',
            }]
          },
          {
            type: 'radio',
            label: '下线状态',
            value: 'offlineFlag',
            children: [{
              label: '未下线',
              value: '0',
            }, {
              label: '已下线',
              value: '1',
            }, {
              label: '暂停',
              value: '2',
            }]
          },
          {
            type: 'radio',
            label: '所属项目',
            value: 'projectId',
            children: [{
              label: '两客一危',
              value: '1',
            }, {
              label: '普货',
              value: '2',
            }]
          }
        ],
        groups: ['001', '002']
      }
    ],
  },
  onFilterChange(e) {
    const self = this;
    const {checkedItems} = e.detail
    const params = {}
    checkedItems.forEach((n) => {
      if (!n.checked) return
      if (n.type === 'sort') {
        params.sortName = n.value
        params.sortDir = n.sort === 1 ? 'asc' : 'desc'
      } else if (n.type === 'filter') {
        n.children.filter(p => p.selected).forEach(x => {
          const checkedItem = x.children.find(m => m.checked)
          if (checkedItem) {
            params[x.value] = checkedItem.value
          }
        })
      }
    })
    this.data.filterParams = params
    this.loadData()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad() {
    await auth.loginInterceptor(helper.getCurrentPagePath())
    this.data.filter.openid = await auth.getOpenid()
    await this.loadData()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  async onPullDownRefresh() {
    await this.loadData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  async onReachBottom() {
    await this.loadData(false)
  },

  /**
   * 搜索事件
   * @param e
   */
  async onSearch(e) {
    console.log(e)
    this.data.filter.key = e.detail.value
    await this.loadData()
  },

  /**
   * 加载数据
   * @param firstPage 是否加载第一页数据
   */
  async loadData(firstPage = true) {
    const data = {...this.data.filter,...this.data.filterParams}
    await this.loadPageData(service.om.selectVehiclePageList, data, firstPage, this.dataListCallback)
  },
  dataListCallback(list) {
    list.forEach(service.om.processVehicleModel)
  },
  onDetail(e) {
    const id = e.currentTarget.dataset.id
    if (!id) return
    wx.navigateTo({
      url: `../vehicle-detail/index?id=${id}`
    })
  }
})