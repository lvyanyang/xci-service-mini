const helper = require('../../xci/helper')
const service = require('../../xci/service')
const app = getApp()
let id

Page({
  data: {
    model: null
  },
  async onLoad(options) {
    id = options.id
    if (!id) return
    const result = await service.om.selectVehicleById(id)
    const model = service.om.processVehicleModel(result.data)
    if (model) {
      this.setData({model})
      wx.setNavigationBarTitle({
        title: `${model.plateNumber} - ${model.customerShortName}`
      })
    } else {
      await helper.showAlert('指定的主键无效')
      wx.navigateBack()
    }
  },
  async onOpenLocation() {
    const result = await service.om.getCarLastGps(id)
    if (!result.success) return
    const latitude = result.data.LatitudeValue
    const longitude = result.data.LongitudeValue
    if (latitude && longitude) {
      app.globalData.vehicle = result.data;
      wx.navigateTo({
        url:'../gpsLocationMarker/index'
      })
      // wx.openLocation({
      //   latitude: Number(latitude),
      //   longitude: Number(longitude)
      // })
    }
  }
})