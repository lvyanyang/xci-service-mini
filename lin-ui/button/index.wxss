.btn-hover::before {
  opacity: 0.15;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 100%;
  height: 100%;
  border: inherit;
  border-radius: inherit;
  /* inherit parent's border radius */
  transform: translate(-50%, -50%);
  content: ' ';
  background-color: #333;
  border-color: #333;
}
.special-container {
  display: flex;
}
.l-btn {
  display: inline-flex;
  align-items: center;
  justify-content: center;
  white-space: nowrap;
  font-size: 28rpx;
  position: relative;
  color: #fff;
  padding: 0 12rpx;
  box-sizing: border-box;
}
.l-btn-img-mini {
  width: 30rpx;
  height: 30rpx;
}
.l-btn-img-medium {
  width: 36rpx;
  height: 36rpx;
}
.l-btn-img-large {
  height: 44rpx;
  width: 44rpx;
}
.l-btn-long {
  border-radius: 0;
  height: 88rpx;
  width: 100%;
}
.l-btn-medium {
  height: 72rpx;
  min-width: 180rpx;
}
.l-btn-large {
  height: 88rpx;
  min-width: 710rpx;
}
.l-btn-mini {
  height: 60rpx;
  min-width: 140rpx;
  font-size: 24rpx;
}
.l-btn-default {
  background-color: #3963BC;
}
.l-btn-success {
  background-color: #34BFA3;
}
.l-btn-warning {
  background-color: #FFE57F;
  color: #333333;
}
.l-btn-error {
  background-color: #F4516C;
}
.l-btn-square {
  border-radius: 0;
}
.l-btn-semicircle {
  border-radius: 40rpx;
}
.l-btn-large.l-btn-semicircle {
  border-radius: 48rpx;
}
.l-btn-mini.l-btn-semicircle {
  border-radius: 30rpx;
}
.l-btn-circle {
  border-radius: 8rpx;
}
.l-btn-large.l-btn-circle {
  border-radius: 9.6rpx;
}
.l-btn-mini.l-btn-circle {
  border-radius: 6rpx;
}
.l-btn-plain {
  background-color: #fff;
  color: #3963BC;
  border: 2rpx solid #3963BC;
}
.l-btn-success.l-btn-plain {
  background-color: #fff;
  color: #34BFA3;
  border: 2rpx solid #34BFA3;
}
.l-btn-error.l-btn-plain {
  background-color: #fff;
  color: #F4516C;
  border: 2rpx solid #F4516C;
}
.l-btn-warning.l-btn-plain {
  background-color: #fff;
  color: #FFE57F;
  border: 2rpx solid #FFE57F;
}
.l-btn-loading {
  opacity: 0.6;
  display: inline-block;
  vertical-align: middle;
  width: 24rpx;
  height: 24rpx;
  background: transparent;
  border-radius: 50%;
  border: 4rpx solid #fff;
  border-color: #fff #fff #fff transparent;
  animation: btn-spin 0.6s linear;
  animation-iteration-count: infinite;
}
.l-btn-loading-default {
  border: 4rpx solid #3963BC;
  border-color: #3963BC #3963BC #3963BC transparent;
}
.l-btn-loading-success {
  border: 4rpx solid #34BFA3;
  border-color: #34BFA3 #34BFA3 #34BFA3 transparent;
}
.l-btn-loading-error {
  border: 4rpx solid #F4516C;
  border-color: #F4516C #F4516C #F4516C transparent;
}
.l-btn-loading-warning {
  border: 4rpx solid #FFE57F;
  border-color: #FFE57F #FFE57F #FFE57F transparent;
}
.l-btn-disabled {
  opacity: 0.8;
}
.icon {
  display: flex !important;
}
.margin-mini {
  margin-right: 10rpx;
}
.margin-medium {
  margin-right: 18rpx;
}
.margin-large {
  margin-right: 24rpx;
}
.margin-long {
  margin-right: 24rpx;
}
@keyframes btn-spin {
  0% {
    transform: rotate(0);
  }
  100% {
    transform: rotate(360deg);
  }
}
